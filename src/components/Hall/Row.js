import React from 'react';
import { Place } from './Place';
//import { cinemaHallsConfig } from '../../config/hallConfig';

export const Row = ( {type, placeCount, hallStyleClass } ) => {

    let row = [];
    for(let i = 1; i <= placeCount; i++) {
        row.push(<Place key={ i } number={ i } cellStyleClass={ hallStyleClass } />);
    }

    return(
        <div className="hall-row">
            {
                row
            }
        </div>
    );

}