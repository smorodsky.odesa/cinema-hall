import React from 'react';

export const Place = ( { number, cellStyleClass } ) => {
    return(
        <div className={"place " + cellStyleClass }>
            { number }
        </div>
    );
}

Place.defaultProps = { cellStyleClass: 'cellDefault' }