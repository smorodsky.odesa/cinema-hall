import './App.css';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import Hall from './pages/Hall';
import { cinemaHallsConfig } from './config/hallConfig'

function App() {
  return (
    <div className="App">
        <BrowserRouter>
            <Link to='/'> Home </Link>

            {/* {
                cinemaHallsConfig.map( (hall, index) => {
                    return(
                    <Link key={index} to={ "/hall/"+hall.name}>{ hall.name }</Link>
                    )
                })
            } */}

            <Switch>
                <Route exact path='/' component={ Home }/>
                <Route exact path='/hall/:hallname' component={ Hall }/>
            </Switch>
        </BrowserRouter>
    </div>
  );
}

export default App;
