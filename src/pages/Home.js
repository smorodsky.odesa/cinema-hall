import React from 'react';

import Hall from './Hall';
import { cinemaHallsConfig } from '../config/hallConfig'
import { Link } from 'react-router-dom';

const Home = () => {
    return(
        <>
            <h1>Home</h1>
            {
                cinemaHallsConfig.map( (hall, index) => {
                    return(

                        <div className="cinema-hall-item">
                            <div>
                                <Link key={index} to={ "/hall/"+hall.name}>
                                    <div key={index} className={ "hall " + hall.name }>
                                        <h1 key={index}> { hall.name } </h1>
                                    </div>
                                </Link>
                            </div>
                        </div>
                    )
                })
            }
        </>
    );
}

export default Home;