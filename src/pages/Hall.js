import React, { useState } from 'react';
import { Row } from '../components/Hall/Row';
import { cinemaHallsConfig } from '../config/hallConfig';

const Hall = ( { match } ) => {
    let hallName = match.params.hallname;
    const [hall, setHall] = useState([]);

    let hallFiltered = cinemaHallsConfig.filter( hall => {         
        return hall.name === hallName;
    });
    hallFiltered = hallFiltered[0];
    
    return(
        <div>
            <h1>{ hallName }</h1>
            <section className="cinema-hall">
                {
                    hallFiltered.hallPlaceMap.map( (row, index) => {
                        return(
                        <div className="row-container">
                            <h2 className={"row-label " + hallName+"-label" }> Ряд {  index+1 }</h2>
                            <Row key={ index }
                                 rowNumber={ index+1 } 
                                 type={ row.type } 
                                 placeCount={ row.placeCount } 
                                 hallStyleClass={ hallName }/>
                        
                        </div>
                        )
                    })
                }
            </section>
        </div>
    );
}

export default Hall;